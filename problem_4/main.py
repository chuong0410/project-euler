import time
import sys
sys.path.append('../utils')
import is_palindrome
import check_number
import traceback


def resolve():
    """
    A palindromic number reads the same both ways. The largest palindrome made
    from the product of two 2-digit numbers is 9009 = 91 × 99.

    Find the largest palindrome made from the product of two 3-digit numbers
    """
    start_time = time.time()
    result = 0
    try:
        for number in range(999 * 999, 100 * 100, -1):
            if check_number.is_prime(number):
                continue
            if not is_palindrome.is_palindrome(number):
                continue
            for i in range(999, 101, -1):
                if number % i == 0 and len(str(number // i)) == 3:
                    result = number
                    break
    except Exception as error:
        traceback.print_exc()
        return False, str(error)
    end_time = time.time()
    return result, "success in {} seconds".format(end_time - start_time)


def main():
    result, message = resolve()
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
