
def get(in_put):
    """
    check string is palindrome
    return False int
    """
    if not isinstance(in_put, list):
        return False
    result = 1
    for i in in_put:
        if not isinstance(in_put, int):
            try:
                i = int(i)
            except Exception:
                return False
        result = result * i
    return result


if __name__ == "__main__":
    import unittest

    class TestStringMethods(unittest.TestCase):
        def test_get(self):
            self.assertEqual(get("aaa"), False)
            self.assertEqual(get([1, 2, 4]), 8)
            self.assertEqual(get([0, 1, 2, 3]), 0)
    unittest.main()
