def get(num):
    """
    get triangle
    return int
    """
    result = num * (num + 1)
    return int(result // 2)


if __name__ == "__main__":
    import unittest

    class TestStringMethods(unittest.TestCase):
        def test_get(self):
            self.assertEqual(get(4), 10)
            self.assertEqual(get(5), 15)
            self.assertEqual(get(7), 28)
            self.assertEqual(get(60), 1830)
    unittest.main()
