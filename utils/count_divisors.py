import math


def count(num):
    """
    count divisors
    return int if not return 0
    """
    result = 0
    for i in range(1, int(math.sqrt(num)) + 1):
        if (num % i == 0):
            if (num / i == i):
                result = result + 1
            else:
                result = result + 2
    return result


if __name__ == "__main__":
    import unittest

    class TestStringMethods(unittest.TestCase):
        def test_count(self):
            self.assertEqual(count(3), 2)
            self.assertEqual(count(15), 4)
            self.assertEqual(count(21), 4)
            self.assertEqual(count(28), 6)
    unittest.main()
