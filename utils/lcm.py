import check_number


def least_common_multiple(a, b):
    """
    find least common multiple two numbers
    return int

    performance: loop from max(a, b) -> a * b with step max(a, b)
    """
    if not isinstance(a, int) or not isinstance(b, int):
        return 0
    if a <= 0 or b <= 0:
        return 0
    result = 0
    if check_number.is_prime(a) and check_number.is_prime(b):
        return a * b
    for i in range(max(a, b), (a * b) + 1, max(a, b)):
        if i % a == 0 and i % b == 0:
            return i
    return result


if __name__ == "__main__":
    import unittest

    class TestStringMethods(unittest.TestCase):
        def test_least_common_multiple(self):
            self.assertEqual(least_common_multiple(0, 2), 0)
            self.assertEqual(least_common_multiple(2, 4), 4)
            self.assertEqual(least_common_multiple(3, 7), 21)
            self.assertEqual(least_common_multiple(84, 108), 756)
            self.assertEqual(least_common_multiple(13, 15), 195)
            self.assertEqual(least_common_multiple(60, 280), 840)
    unittest.main()
