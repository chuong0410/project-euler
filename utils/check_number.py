import math


def is_prime(num):
    """
    check is prime number
    return True False
    """
    if num < 2:
        return False
    if num == 2:
        return True
    if num % 2 == 0:
        return False
    square_root_num = math.sqrt(num)
    if isinstance(square_root_num, int):
        return False
    for i in range(2, int(square_root_num) + 1):
        if num % i == 0:
            return False
    return True


if __name__ == "__main__":
    import unittest

    class TestStringMethods(unittest.TestCase):
        def test_is_prime(self):
            self.assertEqual(is_prime(10), False)
            self.assertEqual(is_prime(49), False)
            self.assertEqual(is_prime(7), True)
            self.assertEqual(is_prime(2), True)
    unittest.main()
