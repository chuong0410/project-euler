

def is_palindrome(in_put):
    """
    check string is palindrome
    return False True
    """
    if not isinstance(in_put, str):
        in_put = str(in_put)
    if len(in_put) == 1:
        return True
    if in_put == in_put[::-1]:
        return True
    return False


if __name__ == "__main__":
    import unittest

    class TestStringMethods(unittest.TestCase):
        def test_is_prime(self):
            self.assertEqual(is_palindrome("aaa"), True)
            self.assertEqual(is_palindrome("aab"), False)
            self.assertEqual(is_palindrome("goog"), True)
    unittest.main()
