import time
import sys
import traceback
sys.path.append('../utils')


def get_produces(num):
    global DICT_CACHE
    old_num = num
    count = 0
    while num != 1:
        if num % 2 == 0:
            num = num // 2
        else:
            num = 3 * num + 1
        if num < old_num:
            DICT_CACHE[old_num] = DICT_CACHE[num] + count
            return DICT_CACHE[old_num]
        count += 1
    DICT_CACHE[old_num] = count
    return count


def resolve(input_number):
    """
    n := n / 2 (n is even)
    n := 3n + 1 (n is odd)
    Which starting number, under one million, produces the longest chain?
    """
    start_time = time.time()
    result = 0
    try:
        tmp = 0
        for num in range(3, input_number + 1):
            produces = get_produces(num)
            if produces > tmp:
                tmp = produces
                result = num
    except Exception as error:
        traceback.print_exc()
        return False, str(error)
    end_time = time.time()
    return result, "success in {} seconds".format(end_time - start_time)


def main(input_number):
    result, message = resolve(input_number)
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    input_number = 1000000
    DICT_CACHE = {x: 0 for x in range(input_number)}
    DICT_CACHE[1] = 1
    DICT_CACHE[2] = 2
    main(input_number)
