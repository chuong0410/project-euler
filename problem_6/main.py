import time
import sys
sys.path.append('../utils')


def resolve(upper_bound):
    """
    Find the difference between the sum of the squares of the
    first one hundred natural numbers and the square of the sum.
    """
    start_time = time.time()
    result = 0
    try:
        sum_squares = 0
        square_sums = 0
        for num in range(1, upper_bound + 1):
            square_sums += num
            sum_squares += num * num
        square_sums = square_sums * square_sums
        result = max(square_sums, sum_squares) - min(square_sums, sum_squares)
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    upper_bound = 100
    result, message = resolve(upper_bound)
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
