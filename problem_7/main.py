import time
import sys
sys.path.append('../utils')
import check_number


def resolve(order):
    """
    By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
    we can see that the 6th prime is 13.

    What is the 10001 st prime number?
    """
    start_time = time.time()
    result = 0
    try:
        temp = []
        num = 1
        while len(temp) <= order - 1:
            if check_number.is_prime(num):
                temp.append(num)
            num += 2
        result = temp[-1]
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    order = 10001
    result, message = resolve(order)
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
