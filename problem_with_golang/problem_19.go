package main

import (
	"fmt"
	"time"
)

// How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?


func Resolve19(day_from string, day_to string) (int, error) {
	res := 0
	layout := "02/01/2006"
    day_from_tmp, err := time.Parse(layout, day_from)
    if err != nil {
        fmt.Println(err)
		return res, err
    }
    // day_from_timestamp := day_from_tmp.Unix()
	// fmt.Println(day_from_timestamp)

    day_to_tmp, err := time.Parse(layout, day_to)
    if err != nil {
        fmt.Println(err)
		return res, err
    }
    // day_to_timestamp := day_to_tmp.Unix()
	// fmt.Println(day_to_timestamp)
	// fmt.Println(day_to_tmp.Weekday())
	// fmt.Println(day_to_tmp.Day())
	
	// calculator
	tmp := day_from_tmp
	for tmp.Before(day_to_tmp) {
		if int(tmp.Weekday()) != 6{
			tmp = tmp.AddDate(0, 0, 1)
			continue
		}
		if int(tmp.Day()) == 1{
			res += 1
		}
		tmp = tmp.AddDate(0, 0, 7)
	}
	return res, nil
}

func main() {
	day_from := "01/01/1901"
	day_to := "31/12/2000"
	sec_before := time.Now()
	result, error_ := Resolve19(day_from, day_to)
	if error_ == nil {
		fmt.Println("Result: ", result)
		sec_after := time.Now()
		fmt.Println("Success in: ", sec_after.Sub(sec_before))
	} else {
		fmt.Println("Somethings went wrong: ", error_)
		sec_after := time.Now()
		fmt.Println("Failed in: ", sec_after.Sub(sec_before))
	}
}
