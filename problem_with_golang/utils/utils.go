package utils

import (
	"math/big"
	"strconv"
	"strings"
)

func ParseToMatrix(data string) ([][]int, error) {
	tmp := strings.Split(data, "\n")
	arrayName := make([][]int, len(tmp))
	for i := 0; i < len(tmp); i++ {
		testArray := strings.Fields(tmp[i])
		arrayName[i] = make([]int, len(tmp))
		for j := 0; j < len(testArray); j++ {
			arrayName[i][j], _ = strconv.Atoi(testArray[j])
		}
	}
	return arrayName, nil
}

func SumBigDigist(number *big.Int) *big.Int {
	ten := big.NewInt(10)
	sum := big.NewInt(0)
	mod := big.NewInt(0)
	for ten.Cmp(number) <= 0 {
		sum.Add(sum, mod.Mod(number, ten))
		number.Div(number, ten)
	}
	return sum.Add(sum, number)
}

func PowBig(a int, x int) *big.Int {
	tmp := big.NewInt(int64(a))
	num_input := big.NewInt(int64(x))
	res := new(big.Int).Exp(tmp, num_input, nil)
	return res
}

func FindMinAndMax(a []int) (min int, max int) {
	min = a[0]
	max = a[0]
	for _, value := range a {
		if value < min {
			min = value
		}
		if value > max {
			max = value
		}
	}
	return min, max
}

func SumArray(array []int) int {
	result := 0
	for _, v := range array {
		result += v
	}
	return result
}

func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

func InitMatrix(lenMatrix int) [][]int {
	result := make([][]int, lenMatrix)
	for i := range result {
		result[i] = make([]int, lenMatrix)
	}
	return result
}

func Factorial(n *big.Int) (result *big.Int) {
    b := big.NewInt(0)
    c := big.NewInt(1)

    if n.Cmp(b) == -1 {
        result = big.NewInt(1)
		return result
    }
    if n.Cmp(b) == 0 {
        result = big.NewInt(1)
    } else {
		result = new(big.Int)
        result.Set(n)
        result.Mul(result, Factorial(n.Sub(n, c)))
    }
    return result
}
