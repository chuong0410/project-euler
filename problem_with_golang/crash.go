// just write somethings
package main

import (
    "fmt"
    "time"
    "strconv"
)

func main() {
    i, err := strconv.ParseInt("1405544146", 10, 64)
    if err != nil {
        panic(err)
    }
    tm := time.Unix(i, 0).Weekday()
    fmt.Println(int(tm) == 7)
    fmt.Println(int(tm))
    fmt.Println(tm)
    fmt.Println(time.Unix(i, 0))
    fmt.Println(int(time.Unix(i, 0).Day()) == 17)
}