package main

import (
	"fmt"
	"time"
	"strings"
	"./utils"
)

// By starting at the top of the triangle below and moving to adjacent numbers on the row below,
// the maximum total from top to bottom is 23.
// 3
// 7 4
// 2 4 6
// 8 5 9 3
// That is, 3 + 7 + 4 + 9 = 23.

func Resolve18(data string) (int, error) {
	// parse data -> array
	Tmp := strings.Split(data, "\n")
	lenTmp := len(Tmp)
	dataParse, error_ := utils.ParseToMatrix(data)
	if error_ != nil {
		return 0, error_
	}
	// end parse

	// calculator
	for i := (lenTmp - 2); i >= 0; i-- {
		for j := 0; j < (lenTmp - 1); j++ {
			dataParse[i][j] = dataParse[i][j] + utils.Max(dataParse[i+1][j], dataParse[i+1][j+1])
		}
	}
	return dataParse[0][0], nil
}

func main() {
	data := `
	75
	95 64
	17 47 82
	18 35 87 10
	20 04 82 47 65
	19 01 23 75 03 34
	88 02 77 73 07 63 67
	99 65 04 28 06 16 70 92
	41 41 26 56 83 40 80 70 33
	41 48 72 33 47 32 37 16 94 29
	53 71 44 65 25 43 91 52 97 51 14
	70 11 33 28 77 73 17 78 39 68 17 57
	91 71 52 38 17 14 91 43 58 50 27 29 48
	63 66 04 68 89 53 67 30 73 16 69 87 40 31
	04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
`
	sec_before := time.Now()
	result, error_ := Resolve18(data)
	if error_ == nil {
		fmt.Println("Result: ", result)
		sec_after := time.Now()
		fmt.Println("Success in: ", sec_after.Sub(sec_before))
	} else {
		fmt.Println("Somethings went wrong: ", error_)
		sec_after := time.Now()
		fmt.Println("Failed in: ", sec_after.Sub(sec_before))
	}
}
