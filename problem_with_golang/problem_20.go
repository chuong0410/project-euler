package main

import (
	"fmt"
	"./utils"
	"time"
	"math/big"
)

// n! means n × (n − 1) × ... × 3 × 2 × 1

// For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
// and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

// Find the sum of the digits in the number 100!

func Resolve20(num_input int) (*big.Int, error) {
	num_input_big := big.NewInt(int64(num_input))
	a := utils.Factorial(num_input_big)
	res := utils.SumBigDigist(a)
	fmt.Println(res)
	
	return res, nil
}

func main() {
	num_input := 100
	result, error_ := Resolve20(num_input)
	sec_before := time.Now()
	if error_ == nil {
		fmt.Println("Result: ", result)
		sec_after := time.Now()
		fmt.Println("Success in: ", sec_after.Sub(sec_before))
	} else {
		fmt.Println("Somethings went wrong: ", error_)
		sec_after := time.Now()
		fmt.Println("Failed in: ", sec_after.Sub(sec_before))
	}
}
