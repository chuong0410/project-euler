package main

import (
	"fmt"
	"./utils"
	"time"
)

// 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
// What is the sum of the digits of the number 2^1000?

func main() {
	num_1 := 2
	num_2 := 1000
	sec_before := time.Now()
	fmt.Println("Result: ", utils.SumBigDigist(utils.PowBig(num_1, num_2)))
	sec_after := time.Now()
	fmt.Println("Success in: ", sec_after.Sub(sec_before))
}
