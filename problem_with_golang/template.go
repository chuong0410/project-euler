package main

import (
	"fmt"
	"time"
)

// some things

func ResolveX(num_input int) (int, error) {
	res := 0
	return res, nil
}

func main() {
	num_input := 100
	sec_before := time.Now()
	result, error_ := ResolveX(num_input)
	if error_ == nil {
		fmt.Println("Result: ", result)
		sec_after := time.Now()
		fmt.Println("Success in: ", sec_after.Sub(sec_before))
	} else {
		fmt.Println("Somethings went wrong: ", error_)
		sec_after := time.Now()
		fmt.Println("Failed in: ", sec_after.Sub(sec_before))
	}
}
