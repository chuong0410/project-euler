package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"./utils"
)

// By starting at the top of the triangle below and moving to adjacent numbers on the row below,
// the maximum total from top to bottom is 23.
// 3
// 7 4
// 2 4 6
// 8 5 9 3
// That is, 3 + 7 + 4 + 9 = 23.

func Resovle67(data string) (int, error) {
	// parse data -> array
	Tmp := strings.Split(data, "\n")
	lenTmp := len(Tmp)
	dataParse, error_ := utils.ParseToMatrix(data)
	if error_ != nil {
		return 0, error_
	}
	// end parse

	// calculator
	for i := (lenTmp - 2); i >= 0; i-- {
		for j := 0; j < (lenTmp - 1); j++ {
			dataParse[i][j] = dataParse[i][j] + utils.Max(dataParse[i+1][j], dataParse[i+1][j+1])
		}
	}
	return dataParse[0][0], nil
}

func main() {
	data, err := ioutil.ReadFile("./p067_triangle.txt")
	if err != nil {
        fmt.Print(err)
    }
	sec_before := time.Now()
	result, error_ := Resovle67(string(data))
	if error_ == nil {
		fmt.Println("Result: ", result)
		sec_after := time.Now()
		fmt.Println("Success in: ", sec_after.Sub(sec_before))
	} else {
		fmt.Println("Somethings went wrong: ", error_)
		sec_after := time.Now()
		fmt.Println("Failed in: ", sec_after.Sub(sec_before))
	}
}
