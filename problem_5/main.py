import time
import sys
sys.path.append('../utils')
import lcm


def resolve(upper_bound):
    """
    2520 is the smallest number that can be divided by each of the
    numbers from 1 to 10 without any remainder.

    What is the smallest positive number that is evenly divisible
    by all of the numbers from 1 to 20?
    - ít nhât số đó phải chia hết cho 10, 9, 7, 13, 11, 17, 19
    """
    start_time = time.time()
    result = 0
    try:
        temp = lcm.least_common_multiple(1, 2)
        for i in range(3, upper_bound + 1):
            temp = lcm.least_common_multiple(temp, i)
        result = temp
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    upper_bound = 20
    result, message = resolve(upper_bound)
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
