import sys
import time
sys.path.append('../utils')
import check_number


def resolve(num):
    """
    The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143 ?
    """
    start_time = time.time()
    result = 0
    try:
        import math
        square_root_num = int(math.sqrt(num))
        for prime_factor in range(1, square_root_num + 1):
            if num % prime_factor != 0:
                continue
            if check_number.is_prime(prime_factor):
                result = prime_factor
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    num = 600851475143
    result, message = resolve(num)
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
