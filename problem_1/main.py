import time


def resolve():
    """
    If we list all the natural numbers below 10 that are
    multiples of 3 or 5, we get 3, 5, 6 and 9.
    The sum of these multiples is 23.

    return: Find the sum of all the multiples of 3 or 5 below 1000
    """
    start_time = time.time()
    multiples = 0
    try:
        for num in range(0, 1000):
            if num % 3 == 0 or num % 5 == 0:
                multiples += num
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return multiples, "success in {} seconds".format(end_time - start_time)


def main():
    result, message = resolve()
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem 1: %s, %s" % (result, message))


if __name__ == '__main__':
    main()
