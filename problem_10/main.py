import time
import sys
sys.path.append('../utils')
from utils import check_number


def resolve(input_data):
    """
    The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

    Find the sum of all the primes below two million.
    """
    start_time = time.time()
    result = 0
    try:
        temp = [num for num in range(3, input_data + 1, 2)
                if check_number.is_prime(num)]
        result = sum(temp) + 2
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    input_data = 2000000
    result, message = resolve(input_data)
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
