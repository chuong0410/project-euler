import time
import sys
sys.path.append('../utils')


def resolve():
    """
    A Pythagorean triplet is a set of three natural numbers, a < b < c
    for which:
    a^2 + b^2 = c^2

    There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    Find the product abc.

    {
        a < b < c;
        natural numbers;
        a^2 + b^2 = c^2
        a + b + c = 1000
    }
    ==> a < b < 500 <= c
    """
    start_time = time.time()
    result = 0
    try:
        for a in range(1, 499):
            for b in range(499, 1, -1):
                if (1000 * (a + b) - (a * b) == 500000):
                    result = a * b * (1000 - a - b)
                    print(a, b, 1000 - a - b)
    except Exception as error:
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    result, message = resolve()
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
