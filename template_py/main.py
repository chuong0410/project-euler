import time
import sys
import traceback
sys.path.append('../utils')


def resolve():
    """
    """
    start_time = time.time()
    result = 0
    try:
        # implement here
        pass
    except Exception as error:
        traceback.print_exc()
        return False, str(error)
    end_time = time.time()

    return result, "success in {} seconds".format(end_time - start_time)


def main():
    result, message = resolve()
    if not result:
        print("resolve failed: {}".format(message))
    print("result problem : %s, %s" % (result, message))


if __name__ == '__main__':
    main()
